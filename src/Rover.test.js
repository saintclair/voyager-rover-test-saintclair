const Rover = require('./Rover');

const perseverance = new Rover('1 2 N', 'LMLMLMLMM');

describe("Create Rover", ()=>{
    test("check attributes for new rover",()=>{

        try{
            const curiosity = new Rover();
        }catch(e){            
            // assert
            expect(e.message).toBe("position_rover and/or instructions is undefined");
        }
        
    });
});

describe("Get final position", ()=>{
    test("adding start position and instructions",()=>{
        
        result = perseverance.getFinalPosition();
        
        // assert
        expect(result).toBe("1 3 N");
    });
});

describe("Get new position", ()=>{
    test("adding position and current instruction",()=>{
        
        result = perseverance.updatePosition('1 2 N','L');

        // assert
        expect(result).toBe("1 2 E");
    });
});

describe("Get new position with wrong instruction", ()=>{
    test("adding position and current instruction",()=>{
        
        result = perseverance.updatePosition('1 2 N','A');

        // assert
        expect(result).toBe("Instruction A unknown.");
    });
});

describe("Get new position with wrong orientation", ()=>{
    test("adding position and current instruction, with wrong orientation",()=>{
        
        result = perseverance.updatePosition('1 2 F','M');

        // assert
        expect(result).toBe("Orientation F unknown.");
    });
});