/**
 * @fileOverview A squad of robotic rovers are to be landed by NASA on a plateau on Mars.
 * @author Saint Clair
 * @Date Sept 2022
 * @version 1.0.0
 */

const orientation_allowed = ['N','S','E','W'];
const instructions_allowed = ['L','R','M'];
const points = 1;

class Rover {
    
    constructor(position_rover, instruction) {
        if(position_rover === undefined || instruction === undefined)
            throw new Error('position_rover and/or instructions is undefined');

        this.position_rover = position_rover;
        this.instruction = instruction;
    }

    /**
     * @method updatePosition
     * @param {String} position_rover
     * @param {String} instruction
     * @returns {String} `${_x} ${_y} ${_orientation}`
     */
    updatePosition(position_rover, instruction) {
        
        let [_x, _y, _orientation] = position_rover.split(' ');
        
        if(instructions_allowed.indexOf(instruction) === -1)
            return `Instruction ${instruction} unknown.`;

        if(orientation_allowed.indexOf(_orientation) === -1)
            return `Orientation ${_orientation} unknown.`;
        
        switch (_orientation) {
            case 'N':
            case 'S':

                if (instruction == 'M') { // Move
                    _y = parseInt(_y);
                    _y += (_orientation == 'N') ? points : -points;

                } else if (instruction == 'L' || instruction == 'R') { // Left, Right
                    _orientation = (_orientation == 'N') ? 'E' : 'W';
                }

                break;
            case 'E':
            case 'W':

                if (instruction == 'M') { // Move
                    _x = parseInt(_x);
                    _x += (_orientation == 'E') ? points : -points;

                } else if (instruction == 'L' || instruction == 'R') { // Left, Right
                    _orientation = (_orientation == 'E') ? 'S' : 'N';
                }

                break;
        }

        return `${_x} ${_y} ${_orientation}`;
    }
    /**
     * @method getFinalPosition
     * @returns {String} position_rover;
     */
    getFinalPosition() {

        let position_rover = '';

        // Chain string (exploration)
        for (let step in this.instruction) {

            let instruction = this.instruction[step];
            
            if (step == 0) {
                position_rover = this.updatePosition(this.position_rover, instruction);
                continue;
            }
            position_rover = this.updatePosition(position_rover, instruction);

        }
        return position_rover;
    }

}

module.exports = Rover;

