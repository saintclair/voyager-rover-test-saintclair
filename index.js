const Rover = require('./src/Rover');

try{
    const perseverance = new Rover('1 2 N', 'LMLMLMLMM');
    console.log(perseverance.getFinalPosition());
}catch(e){
    console.log(e.message);
}

try{
    const curiosity = new Rover('3 3 E', 'MRRMMRMRRM');
    console.log(curiosity.getFinalPosition());
}catch(e){
    console.log(e.message);
}