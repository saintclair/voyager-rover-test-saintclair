# Mars Rover in JavaScript

## ✔️ Requirements
[https://gitlab.com/voyager-portal/voyager-rover-test](https://gitlab.com/voyager-portal/voyager-rover-test)

## 🎁 Bônus

### Pipeline Status (CI/CD)

I configured the CI/CD on gitlab for running tests always that to happen a push on the branch. (simple file: .gitlab-ci.yml)

### 🛠 Tools

- [NodeJs](https://nodejs.org/)
- [Jest](https://jestjs.io/)

### 🎲 Running the project

```
    # Clone the Repo
    $ git clone <https://gitlab.com/saintclair/voyager-rover-test-saintclair.git>

    # Install packages
    $ npm install

    # Run the main code
    $ npm start

    # Run the suite test (Jest)
    $ npm test
```

### Author

[![Linkedin Badge](https://img.shields.io/badge/-Saint%20Clair-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/websaintclair/)](https://www.linkedin.com/in/websaintclair/)

[![Gmail Badge](https://img.shields.io/badge/-saintclair.com@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:saintclair.com@gmail.com)](mailto:saintclair.com@gmail.com)